pipeline {
    agent {
        node {
            label 'maven'
        }

    }
    stages {
        stage('拉取代码') {
            agent none
            steps {
                container('base') {
                    git(url: 'https://gitee.com/jzgr/jenkinsDemo.git', credentialsId: 'gitee', branch: 'master', changelog: true, poll: false)
                }

            }
        }

        stage('测试') {
            agent none
            steps {
                container('maven') {
                    sh 'mvn clean  test'
                }

            }
        }

        stage('代码分析') {
            agent none
            steps {
                container('maven') {
                    withCredentials([string(credentialsId: 'sonar-token', variable: 'SONAR_TOKEN',)]) {
                        withSonarQubeEnv('sonar') {
                            sh 'mvn sonar:sonar -Dsonar.login=$SONAR_TOKEN'
                        }

                    }

                    timeout(unit: 'HOURS', activity: true, time: 1) {
                        waitForQualityGate 'true'
                    }

                }

            }
        }

        stage('构建项目') {
            agent none
            steps {
                container('maven') {
                    sh 'mvn clean package -Dmaven.test.skip=true'
                    sh 'ls'
                }

            }
        }

        stage('制作镜像') {
            agent none
            steps {
                container('maven') {
                    sh 'docker build -f Dockerfile -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BUILD_NUMBER .'
                    withCredentials([usernamePassword(credentialsId: 'dockerhub-id', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME',)]) {
                        sh 'echo "$DOCKER_PASSWORD" | docker login --username="$DOCKER_USERNAME" $REGISTRY --password-stdin'
                        sh 'docker push $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BUILD_NUMBER'
                    }

                }

            }
        }

        stage('保存制品') {
            agent none
            steps {
                archiveArtifacts 'target/*.jar'
            }
        }

        stage('部署项目') {
            agent none
            steps {
                input(message: '@jzgr  部署？', submitter: 'jzgr')
                withCredentials([
                    kubeconfigContent(
                        credentialsId: 'demo-kubeconfig',
                        variable: 'KUBECONFIG_CONTENT'
                    )
                ]) {
                    sh 'envsubst < deploy/deploy.yaml | kubectl apply -f -'
                }
            }
        }

    }
    environment {
        DOCKER_CREDENTIAL_ID = 'dockerhub-id'
        GITHUB_CREDENTIAL_ID = 'github-id'
        KUBECONFIG_CREDENTIAL_ID = 'demo-kubeconfig '
        REGISTRY = 'registry.cn-hangzhou.aliyuncs.com'
        DOCKERHUB_NAMESPACE = 'jzgr'
        GITHUB_ACCOUNT = 'jzgr'
        APP_NAME = 'demo-java'
    }
}