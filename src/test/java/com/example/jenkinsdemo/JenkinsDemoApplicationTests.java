package com.example.jenkinsdemo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class JenkinsDemoApplicationTests {

    @Autowired
    private JenkinsDemoApplication jenkinsDemoApplication;

    /**
     * 测试用例
     */
    @Test
    void contextLoads() {
        String test = jenkinsDemoApplication.test();
        Object o = new Object();
        Assertions.assertNotNull(o,test);
    }

}
